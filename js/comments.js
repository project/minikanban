/**
 * @file
 * Prepare and send data to augmentor execute functions and handle the response.
 */

(function (Drupal, $) {
  $(document).ready(function () {
    $('#edit-submit').on('click', function (e) {
      let editors = Drupal.CKEditor5Instances.entries();
      for (let [x, editor] of editors) {
        if (editor.sourceElement.id === 'edit-comment-0-value') {
          editor.setData('');
        }
      }
    });
  });
})(Drupal, jQuery);
