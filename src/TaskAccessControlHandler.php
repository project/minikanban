<?php declare(strict_types = 1);

namespace Drupal\minikanban;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the task entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class TaskAccessControlHandler extends KanbanAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    $parentReturn = parent::checkAccess($entity, $operation, $account);
    if (!$parentReturn->isNeutral()) {
      return $parentReturn;
    }
    // If the person can update status.
    return match($operation) {
      'view' => AccessResult::allowedIfHasPermissions($account, ['view kanban_task', 'administer kanban_task'], 'OR'),
      'update' => AccessResult::allowedIfHasPermissions($account, ['edit kanban_task', 'administer kanban_task'], 'OR'),
      'delete' => AccessResult::allowedIfHasPermissions($account, ['delete kanban_task', 'administer kanban_task'], 'OR'),
      'update-task-status' => AccessResult::allowedIfHasPermissions($account, ['edit kanban_task', 'update kanban_task status', 'administer kanban_task'], 'OR'),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create kanban_task', 'administer kanban_task'], 'OR');
  }

}
