<?php declare(strict_types = 1);

namespace Drupal\minikanban\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\minikanban\CommentInterface;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the comment entity class.
 *
 * @ContentEntityType(
 *   id = "kanban_comment",
 *   label = @Translation("Comment"),
 *   label_collection = @Translation("Comments"),
 *   label_singular = @Translation("comment"),
 *   label_plural = @Translation("comments"),
 *   label_count = @PluralTranslation(
 *     singular = "@count comments",
 *     plural = "@count comments",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\minikanban\CommentListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\minikanban\Form\CommentForm",
 *       "edit" = "Drupal\minikanban\Form\CommentForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "kanban_comment",
 *   revision_table = "kanban_comment_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer kanban_comment",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/kanban-comment",
 *     "add-form" = "/kanban/comment/add",
 *     "canonical" = "/kanban/comment/{kanban_comment}",
 *     "edit-form" = "/kanban/comment/{kanban_comment}/edit",
 *     "delete-form" = "/kanban/comment/{kanban_comment}/delete",
 *     "delete-multiple-form" = "/admin/content/kanban-comment/delete-multiple",
 *   },
 * )
 */
final class Comment extends RevisionableContentEntityBase implements CommentInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    // Check if new.
    if (!$update && \Drupal::config('minikanban.settings')->get('send_on_comment')) {
      // Send email.
      $mailSender = \Drupal::service('minikanban.mailsender');
      $parentEntity = $this->getTask() ?? $this->getEpic();
      // Set html.
      $html = $this->comment->value;
      $html .= '<br><br><hr><a href="' . $parentEntity->toUrl()->toString() . '">View comment</a>';

      foreach ($parentEntity->getWatchers() as $watcher) {
        // Only send if they have one.
        $email = $watcher->getEmail();
        if ($email) {
          $mailSender->sendEmail(
            $email,
            t('@author commented on @type "@name (@id)"', [
              '@id' => $this->id(),
              '@type' => $parentEntity->getEntityType()->getLabel(),
              '@name' => $parentEntity->label(),
              '@author' => $this->getOwner()->getDisplayName(),
            ]),
            $html,
          );
        }
      }
    }
    // Update the task or epic to recount.
    if ($this->getTask()) {
      $this->getTask()->save();
    }
    if ($this->getEpic()) {
      $this->getEpic()->save();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the comment was created.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the comment was last edited.'));

    $fields['project'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Project'))
      ->setSetting('target_type', 'kanban_project')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 10,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['task'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Task'))
      ->setSetting('target_type', 'kanban_task')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 10,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['epic'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Epic'))
      ->setSetting('target_type', 'kanban_epic')
      ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'weight' => 10,
        ])
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'author',
          'weight' => 20,
        ])
        ->setDisplayConfigurable('view', TRUE);

    $fields['comment'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Add Comment'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
        'settings' => [
          'rows' => 2,
          'default_format' => 'kanban_html',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Get task.
   *
   * @return \Drupal\minikanban\Entity\Task|null
   *   The task.
   */
  public function getTask(): Task|null {
    return $this->get('task')->entity ?? NULL;
  }

  /**
   * Get epic.
   *
   * @return \Drupal\minikanban\Entity\Epic|null
   *   The epic.
   */
  public function getEpic(): Epic|null {
    return $this->get('epic')->entity ?? NULL;
  }

  /**
   * Gets the comment.
   *
   * @return string
   *   The comment.
   */
  public function getComment(): string {
    return $this->comment->value;
  }

  /**
   * Get commenters username.
   *
   * @return string
   *   The commenters username.
   */
  public function getAuthorsUsername(): string {
    return $this->getOwner()->getDisplayName();
  }

}
