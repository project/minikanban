<?php

namespace Drupal\minikanban;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Class MailSender.
 */
class MailSender {

  /**
   * The mail manager.
   *
   * @var \Drupal\Core\Mail\MailManagerInterface
   */
  protected $mailManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new ProjectHelper object.
   *
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   The mail manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   */
  public function __construct(MailManagerInterface $mailManager, AccountProxyInterface $currentUser) {
    $this->mailManager = $mailManager;
    $this->currentUser = $currentUser;
  }

  /**
   * Send email.
   *
   * @param string $to
   *   The email address to send to.
   * @param string $subject
   *   The email subject.
   * @param string $body
   *   The email body.
   */
  public function sendEmail($to, $subject, $body) {
    $params = [
      'to' => $to,
      'from' => $this->currentUser->getEmail(),
      'subject' => $subject,
      'message' => $body,
    ];
    $this->mailManager->mail('minikanban', 'minikanban_notification', $to, $this->currentUser->getPreferredLangcode(), $params);
  }

}
