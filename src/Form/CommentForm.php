<?php declare(strict_types = 1);

namespace Drupal\minikanban\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\minikanban\Entity\Epic;
use Drupal\minikanban\Entity\Task;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for the comment entity edit forms.
 */
final class CommentForm extends ContentEntityForm implements ContainerInjectionInterface {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new CommentForm object.
   */
  public function __construct(RouteMatchInterface $route_match, EntityRepositoryInterface $entity_repository, EntityTypeBundleInfoInterface $entity_type_bundle_info, TimeInterface $time) {
    parent::__construct($entity_repository, $entity_type_bundle_info, $time);
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('entity.repository'),
      $container->get('entity_type.bundle.info'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    $form['#attached']['library'][] = 'minikanban/comments';
    $form['comment']['widget'][0]['#format'] = 'kanban_html';
    $form['comment']['widget'][0]['#allowed_formats'] = ['kanban_html'];
    foreach ($this->routeMatch->getParameters() as $parameter) {
      /** @var \Drupal\minikanban\Entity\Task */
      if ($parameter instanceof Task) {
        // Load the task id.
        $form['task']['widget'][0]['target_id']['#default_value'] = $parameter;
        // Load the project id.
        $form['project']['widget'][0]['target_id']['#default_value'] = $parameter->getProject();
      }

      if ($parameter instanceof Epic) {
        // Load the epic id.
        $form['epic']['widget'][0]['target_id']['#default_value'] = $parameter;
        // Load the project id.
        $form['project']['widget'][0]['target_id']['#default_value'] = $parameter->getProject();
      }
    }
    // Hide stuff.
    $form['task']['#access'] = FALSE;
    $form['epic']['#access'] = FALSE;
    $form['project']['#access'] = FALSE;

    // Hide revision on creation form.
    if ($this->entity->isNew()) {
      $form['revision']['#access'] = FALSE;
      $form['revision_information']['#access'] = FALSE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $form = parent::actions($form, $form_state);
    $form['submit']['#value'] = $this->t('Create comment');
    $form['submit']['#ajax'] = [
      'callback' => '::updateViews',
      'event' => 'mousedown',
      'prevent' => 'click',
      'progress' => [
        'type' => 'throbber',
        'message' => $this->t('Creating comment...'),
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $message_args = ['%label' => $this->entity->toLink()->toString()];
    $logger_args = [
      '%label' => $this->entity->label(),
      'link' => $this->entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New comment %label has been created.', $message_args));
        $this->logger('minikanban')->notice('New comment %label has been created.', $logger_args);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The comment %label has been updated.', $message_args));
        $this->logger('minikanban')->notice('The comment %label has been updated.', $logger_args);
        break;

      default:
        throw new \LogicException('Could not save the entity.');
    }

    $form_state->setRedirectUrl($this->entity->toUrl());

    return $result;
  }

  /**
   * Ajax callback for the submit button to update the views.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function updateViews(array $form, FormStateInterface $formState) {
    $response = new AjaxResponse();
    // Refresh the comments list.
    $response->addCommand(new InvokeCommand('.view-kanban-comments-list', 'trigger', ['RefreshView']));
    return $response;
  }

}
