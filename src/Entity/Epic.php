<?php declare(strict_types = 1);

namespace Drupal\minikanban\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\minikanban\EpicInterface;
use Drupal\user\Entity\User;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the epic entity class.
 *
 * @ContentEntityType(
 *   id = "kanban_epic",
 *   label = @Translation("Epic"),
 *   label_collection = @Translation("Epics"),
 *   label_singular = @Translation("epic"),
 *   label_plural = @Translation("epics"),
 *   label_count = @PluralTranslation(
 *     singular = "@count epics",
 *     plural = "@count epics",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\minikanban\EpicListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\minikanban\EpicAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\minikanban\Form\EpicForm",
 *       "edit" = "Drupal\minikanban\Form\EpicForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "kanban_epic",
 *   admin_permission = "administer kanban_epic",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/kanban-epic",
 *     "add-form" = "/kanban/epic/add",
 *     "canonical" = "/kanban/epic/{kanban_epic}",
 *     "edit-form" = "/kanban/epic/{kanban_epic}/edit",
 *     "delete-form" = "/kanban/epic/{kanban_epic}/delete",
 *     "delete-multiple-form" = "/admin/content/kanban-epic/delete-multiple",
 *   },
 *   field_ui_base_route = "entity.kanban_epic.settings",
 * )
 */
final class Epic extends ContentEntityBase implements EpicInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  public static array $epicStatuses = [
    'backlog' => 'Backlog',
    'todo' => 'To do',
    'in_progress' => 'In progress',
    'in_review' => 'In review',
    'done' => 'Done',
    'closed' => 'Closed',
  ];

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    // Check if new.
    if (!$update && \Drupal::config('minikanban.settings')->get('send_on_created')) {
      // Send email.
      $mailSender = \Drupal::service('minikanban.mailsender');
      // Set html.
      $html = $this->description->value;
      $html .= '<br><br><hr><a href="' . $this->toUrl()->toString() . '">View epic</a>';
      foreach ($this->getWatchers() as $watcher) {
        // Only send if they have one.
        $email = $watcher->getEmail();
        if ($email) {
          $title = $watcher == $this->getAssigned() ? '@author assigned you epic "@epic (@id)"' : 'Epic "@epic (@id)" created by @author';
          $mailSender->sendEmail(
            $email,
            t($title, [
              '@id' => $this->id(),
              '@epic' => $this->label(),
              '@author' => $this->getOwner()->getDisplayName(),
            ]),
            $html,
          );
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    $textHelper = $this->kanbanTextHelper();
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }

    // Get comments.
    $comments = $this->getComments();

    // Set attachments from comments and epic.
    $files = $textHelper->grabFilesFromText($this->description->value);
    foreach ($comments as $comment) {
      $files = array_merge($files, $textHelper->grabFilesFromText($comment->comment->value));
    }
    $this->set('attachements', $files);

    // Find watchers.
    $watchers = [
      $this->getOwnerId(),
      $this->assigned->target_id
    ];

    // From description and comments.
    $watchers = array_merge($textHelper->grabUsersFromText($this->description->value), $watchers);
    foreach ($comments as $comment) {
      $watchers = array_merge($watchers, $textHelper->grabUsersFromText($comment->comment->value));
    }
    $this->set('watchers', array_unique($watchers));
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    parent::delete();
    // Delete comments on task deletion.
    foreach ($this->getComments() as $comment) {
      $comment->delete();
    }
    // Delete tasks on task deletion.
    foreach ($this->getTasks() as $task) {
      $task->delete();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the epic was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the epic was last edited.'));

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 20,
        'settings' => [
          'rows' => 5,
          'default_format' => 'kanban_html',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['assigned'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Assigned to'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 19,
      ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'author',
          'weight' => 30,
        ])
        ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setDefaultValue('backlog')
      ->setSettings([
        'allowed_values' => self::$epicStatuses,
      ])
        ->setDisplayOptions('form', [
          'type' => 'options_select',
          'weight' => 15,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'string',
          'weight' => 15,
        ])
        ->setDisplayConfigurable('view', TRUE);

    $fields['watchers'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Watchers'))
      ->setSetting('target_type', 'user')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'author',
          'weight' => 35,
        ])
        ->setDisplayConfigurable('view', TRUE);

    $fields['project'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Project'))
      ->setSetting('target_type', 'kanban_project')
      ->setRequired(TRUE)
        ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
            'match_operator' => 'CONTAINS',
            'size' => 60,
            'placeholder' => '',
          ],
          'weight' => 20,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'kanban_project',
          'weight' => 20,
        ])
        ->setDisplayConfigurable('view', TRUE);

    $fields['background_color'] = BaseFieldDefinition::create('color_field_type')
      ->setLabel(t('Marked color'))
      ->setSettings([
        'format', '#HEXHEX',
        'opacity' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'color_field_widget_html5',
        'weight' => 25,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'color',
        'weight' => 25,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['attachements'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Attachements'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setSetting('file_extensions', 'txt docx doc docm dot dotm dotx odt pdf xml csv ods xls xlsb xlsm xlsx xlt xltx bmp jpg gif png jpeg odp pot ppa ppam pps ppt pptx rtf mp4 mov avi flv webm wmv zip tar mp3 aac flac wav wma')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'file_default',
        'weight' => 60,
        'settings' => [
          'use_description_as_link_text' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Gets the project.
   *
   * @return \Drupal\minikanban\Entity\Project|null
   *   The project.
   */
  public function getProject(): Project|null {
    return $this->project->entity ? $this->project->entity : NULL;
  }

  /**
   * Gets the assigned person.
   *
   * @return \Drupal\user\Entity\User|null
   *   The assigned person.
   */
  public function getAssigned(): User|null {
    return $this->assigned->entity ? $this->assigned->entity : NULL;
  }

  /**
   * Gets the tasks connected to the epic.
   */
  public function getTasks(): array {
    $tasks = [];
    $epicTasks = $this->entityTypeManager()->getStorage('kanban_task')->loadByProperties(['epic' => $this->id()]);
    foreach ($epicTasks as $task) {
      $tasks[] = $task;
    }
    return $tasks;
  }

  /**
   * Gets the Kanban Text Helper.
   *
   * @return \Drupal\minikanban\TextHelper
   */
  protected function kanbanTextHelper() {
    return \Drupal::service('minikanban.text_helper');
  }

  /**
   * Gets the watchers.
   *
   * @return array
   *   The watchers.
   */
  public function getWatchers(): array {
    $watchers = [];
    foreach ($this->watchers as $watcher) {
      $watchers[] = $watcher->entity;
    }
    return $watchers;
  }

  /**
   * Gets the comments for an epic.
   *
   * @return array
   *   The comments for an epic.
   */
  public function getComments(): array {
    $comments = [];
    $commentStorage = $this->entityTypeManager()->getStorage('kanban_comment');
    $query = $commentStorage->getQuery()
      ->condition('epic', $this->id())
      ->accessCheck(TRUE);
    $comment_ids = $query->execute();
    if ($comment_ids) {
      $comments = $commentStorage->loadMultiple($comment_ids);
    }
    return $comments;
  }

  /**
   * Adds a comment.
   *
   * @param string $comment
   *   The comment to add.
   * @param int $uid
   *   The user id of the author.
   *
   * @return \Drupal\minikanban\Entity\Comment
   *   The comment entity.
   */
  public function addComment(string $comment, $uid) {
    $commentStorage = $this->entityTypeManager()->getStorage('kanban_comment');
    $comment = $commentStorage->create([
      'epic' => $this->id(),
      'uid' => $uid,
      'comment' => [
        'value' => $comment,
        'format' => 'kanban_html',
      ],
    ])->save();
    return $comment;
  }

  /**
   * Get title.
   *
   * @return string
   *   The title.
   */
  public function getTitle(): string {
    return $this->label();
  }

  /**
   * Get author name.
   *
   * @return string
   *   The author name.
   */
  public function getAuthorsUsername(): string {
    return $this->getOwner()->getDisplayName();
  }

  /**
   * Get the author.
   *
   * @return \Drupal\user\Entity\User
   *   The author.
   */
  public function getAuthor(): User {
    return $this->getOwner();
  }

  /**
   * Get description.
   *
   * @return string
   *   The description.
   */
  public function getDescription(): string {
    return $this->description->value;
  }

  /**
   * Gets the background color.
   *
   * @return string
   *   The background color.
   */
  public function getBackgroundColor(): string {
    return $this->background_color->color ?? '#000000';
  }

  /**
   * Sets the epic status.
   *
   * @param string $status
   *   The status.
   */
  public function setStatus(string $status): void {
    $this->set('status', $status);
  }

  /**
   * Set the assignee of the task.
   *
   * @param \Drupal\user\Entity\User $user
   *   The user to assign the task to.
   */
  public function setAssignee(User $user): void {
    $this->set('assigned', $user->id());
  }

}
