<?php

namespace Drupal\minikanban\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;

class EpicBreadcrumbs implements BreadcrumbBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return $route_match->getRouteName() == 'entity.kanban_epic.canonical';
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $epic = $route_match->getParameter('kanban_epic');
    $breadcrumb->addLink(Link::createFromRoute('Home', '<front>'));
    $breadcrumb->addLink(Link::createFromRoute('Kanban', 'minikanban.kanban_board'));
    $project = $epic->getProject();
    $breadcrumb->addLink(Link::createFromRoute($project->label(), 'entity.kanban_project.canonical', ['kanban_project' => $project->id()]));
    return $breadcrumb;
  }

}
