<?php

namespace Drupal\minikanban;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\minikanban\Entity\Task;

/**
 * Class TaskHelper.
 */
class TaskHelper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logged in user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Constructs a new TaskHelper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The logged in user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
  }

  /**
   * Update the status of a task.
   *
    * @param \Drupal\minikanban\Entity\Task $task
   *   The task.
   * @param string $status
   *   The status.
   * @param int $weight
   *   The weight.
   *
   * @return \Drupal\minikanban\Entity\Task
   *   The task.
   */
  public function updateTaskStatus(Task $task, $status, $weight = NULL) {

    $task = $this->entityTypeManager->getStorage('kanban_task')->load($task->id());
    if (!$task->access('update-task-status', $this->currentUser)) {
      throw new \Exception('Access denied');
    }
    $task->set('status', $status);
    $task->set('weight', is_null($weight) ? $task->weight->value : $weight);
    $task->save();
    return $task;
  }

  /**
   * Update task status without access check.
   *
   * @param \Drupal\minikanban\Entity\Task $task
   *   The task.
   * @param string $status
   *   The status.
   * @param int $weight
   *   The weight.
   *
   * @return \Drupal\minikanban\Entity\Task
   *   The task.
   */
  public function updateTaskStatusWithoutAccessCheck(Task $task, $status, $weight = NULL) {
    $task->set('status', $status);
    $task->set('weight', is_null($weight) ? $task->weight->value : $weight);
    $task->save();
    return $task;
  }

}
