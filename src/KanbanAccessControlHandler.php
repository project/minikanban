<?php

declare(strict_types=1);

namespace Drupal\minikanban;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Overriding access control for project base.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
class KanbanAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    // Administrators or special solution sees all.
    if ($account->hasPermission('administer kanban')) {
      return AccessResult::allowed();
    }
    if ($account->hasPermission('view all kanban projects')) {
      return AccessResult::neutral();
    }
    // If the entity does not have a project, then it is not accessible.
    if (!$entity->hasField('project') || !$entity->get('project')->entity) {
      return AccessResult::forbidden();
    }
    // If the user is a member of the project, then they can see the entity.
    foreach ($entity->get('project')->entity->members as $member) {
      if ($member->entity->id() === $account->id()) {
        return AccessResult::neutral();
      }
    }
    return AccessResult::forbidden();
  }

}
