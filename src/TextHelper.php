<?php

namespace Drupal\minikanban;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class TextHelper.
 */
class TextHelper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new TaskHelper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get files from Wysiwig field.
   *
   * @param string $text
   *
   * @return array
   *   Array of files.
   */
  public function grabFilesFromText($text) {
    $files = [];
    $matches = [];
    preg_match_all('/<a[^>]+data-entity-uuid="([^">]+)"/', $text, $matches);
    foreach ($matches[1] as $match) {
      $file = $this->entityTypeManager->getStorage('file')->loadByProperties(['uuid' => $match]);
      if ($file) {
        $files[] = reset($file);
      }
    }
    $matches = [];
    preg_match_all('/<img[^>]+data-entity-uuid="([^">]+)"/', $text, $matches);
    foreach ($matches[1] as $match) {
      $file = $this->entityTypeManager->getStorage('file')->loadByProperties(['uuid' => $match]);
      if ($file) {
        $files[] = reset($file);
      }
    }
    return $files;
  }

  /**
   * Grab users with @ sign from text.
   *
   * @param string $text
   *   The text.
   *
   * @return array
   *   The users.
   */
  public function grabUsersFromText($text) {
    $users = [];
    $matches = [];
    preg_match_all('/<a[^>]+data-entity-uuid="([^">]+)"/', $text, $matches);
    foreach ($matches[1] as $match) {
      $user = $this->entityTypeManager->getStorage('user')->loadByProperties(['uuid' => $match]);
      if ($user) {
        $users[] = key($user);
      }
    }
    return $users;
  }

}
