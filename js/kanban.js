(function ($, drupalSettings, Drupal) {
  let moving = false;
  let started = false;

  // Listen for dialog close event.
  Drupal.behaviors.myCustomBehavior = {
    listenerOn: false,
    attach: function (context, settings) {
      if (!this.listenerOn) {
        this.listenerOn = true;
        $(window).on('dialog:afterclose', function (event) {
          moving = false;
          reloadStatus();
        });
        $(window).on('dialog:aftercreate', function (event) {
          moving = true;
        });
      }
    }
  };

  $(document).ready(function () {
    // Set events.
    setEvents();
    // Start reloading every 5 seconds.
    setInterval(reloadStatus, 2000);
  });

  function setEvents() {
    $(".kanban-sortable").sortable({
      connectWith: ".kanban-sortable",
      over: kanbanOver,
      stop: kanbanStop,
      start: kanbanStart,
      receive: kanbanReceive,
    });
    $('.kanban-task-filter-form').on('mouseover', function() {
      moving = true;
    });
    $('.kanban-task-filter-form').on('mouseout', function() {
      moving = false;
    });
  }

  // Reload the status of the tasks.
  function reloadStatus() {
    if (moving) {
      return;
    }
    let project = drupalSettings.kanban.project ? '/' + drupalSettings.kanban.project : '';
    let url = drupalSettings.path.baseUrl + 'kanban/reload-task-status' + project;
    $.ajax(url).always(function(data) {
      if ("responseJSON" in data && (!("status" in data.responseJSON) || data.responseJSON.status == 'error')) {
        setMessage(data.responseJSON.message, 'error');
      }
      else {
        $('#kanban-board #board').html(data.board);
        // Rebuild the ajax links after load.
        Drupal.ajax.bindAjaxLinks(document.getElementById('kanban-board'));
        // Reset jquery ui sortable.
        setEvents();
      }
    });
  }

  // Start the event and let know not to update.
  function kanbanStart(event, ui) {
    moving = true;
    started = true;
  }

  // Stop the event and remove the hover effect.
  function kanbanStop(event, ui) {
    // If we move in the same place.
    if (started) {
      recalculate(event, ui);
      started = false;
    }
    $('.kanban-block').removeClass('kanban-hover');
    moving = false;
  }

  function kanbanReceive(event, ui) {
    started = false;
    recalculate(event, ui);
  }

  // Recalculate.
  function recalculate(event, ui) {
    if ($(ui.item).hasClass('kanban-epic')) {
      recalculateEpics(event);
    }
    else {
      recalculateTasks(event);
    }
  }

  // Recalculate the epics.
  function recalculateEpics(event) {
    let weights = reweight($(event.target).find('.kanban-epic'));
    let status = $(event.target).data('status');
    let epics = [];
    for (let weight in weights) {
      epics.push({
        'id': weights[weight],
        'status': status,
      })
    }
    $.post(drupalSettings.path.baseUrl + 'kanban/update-epic-status-multiple', {
      'epics': epics,
    }).always(function (data) {
      if ("responseJSON" in data && (!("status" in data.responseJSON) || data.responseJSON.status == 'error')) {
        setMessage(data.responseJSON.message, 'error');
      }
    });
  }

  // Recalculate the tasks.
  function recalculateTasks(event) {
    let status = $(event.target).data('status');
    let weights = reweight($(event.target).find('.kanban-task'));
    let tasks = [];
    for (let weight in weights) {
      tasks.push({
        'id': weights[weight],
        'status': status,
        'weight': weight,
      })
    }
    $.post(drupalSettings.path.baseUrl + 'kanban/update-task-status-multiple', {
      'tasks': tasks,
    }).always(function (data) {
      if ("responseJSON" in data && (!("status" in data.responseJSON) || data.responseJSON.status == 'error')) {
        setMessage(data.responseJSON.message, 'error');
      }
    });
  }

  // Reweight the tasks.
  function reweight(tasks) {
    console.log(tasks);
    let weights = [];
    tasks.each(function(index) {
      weights.push($(this).data('task-id'));
    });
    return weights;
  }

  // Make hover effect.
  function kanbanOver( event, ui ) {
    $('.kanban-block').removeClass('kanban-hover');
    $(event.target).parent().addClass('kanban-hover');
  }

  // Set message with timeout.
  function setMessage(message, type) {
    let messages = new Drupal.Message();
    messages.add(message, { type: type });
    // Automatically clear the message after 5 seconds.
    setTimeout(function() {
      messages.clear();
    }, 5000);
  }
})(jQuery, drupalSettings, Drupal);
