<?php

namespace Drupal\minikanban\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\minikanban\ProjectHelper;

/**
 * Class TaskFilterForm.
 */
class TaskFilterForm extends FormBase implements ContainerInjectionInterface {

  /**
   * The project helper.
   *
   * @var \Drupal\minikanban\ProjectHelper
   */
  protected $projectHelper;

  /**
   * Constructs a new TaskFilterForm object.
   */
  public function __construct(ProjectHelper $project_helper) {
    $this->projectHelper = $project_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(\Symfony\Component\DependencyInjection\ContainerInterface $container) {
    return new static(
      $container->get('minikanban.project_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'kanban_task_filter_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $formState) {
    $form['project'] = [
      '#type' => 'select',
      '#title' => $this->t('Project'),
      '#options' => ['' => $this->t('- Any -')] + $this->projectHelper->getProjectsForOptions(),
      '#default_value' => $this->getRouteMatch()->getParameter('project') ? $this->getRouteMatch()->getParameter('project')->id() : '',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Filter'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    if ($formState->getValue('project')) {
      $formState->setRedirect('minikanban.kanban_board_project', [
        'project' => $formState->getValue('project'),
      ]);
    }
    else {
      $formState->setRedirect('minikanban.kanban_board');
    }
  }

}

