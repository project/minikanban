<?php

namespace Drupal\minikanban;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Class ProjectHelper.
 */
class ProjectHelper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new ProjectHelper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get all projects.
   *
   * @return array
   *   The projects.
   */
  public function getProjects() {
    $storage = $this->entityTypeManager->getStorage('kanban_project');
    $query = $storage->getQuery();
    $query->sort('created');
    $query->accessCheck(TRUE);
    $project_ids = $query->execute();
    return $storage->loadMultiple($project_ids);
  }

  /**
   * Get projects for options.
   *
   * @return array
   *   The projects for options.
   */
  public function getProjectsForOptions() {
    $projects = $this->getProjects();
    $options = [];
    foreach ($projects as $project) {
      $options[$project->id()] = $project->label();
    }
    return $options;
  }

  /**
   * Get the project.
   *
   * @param int $project_id
   *   The project id.
   *
   * @return \Drupal\minikanban\Entity\Project
   *   The project.
   */
  public function getProject($project_id) {
    return $this->entityTypeManager->getStorage('kanban_project')->load($project_id);
  }

  /**
   * Get all tasks for a project.
   *
   * @param int $project_id
   *   The project id.
   * @param array $statuses
   *   The statuses.
   *
   * @return array
   *   The tasks.
   */
  public function getTasks($project_id = NULL, $statuses = NULL) {
    $storage = $this->entityTypeManager->getStorage('kanban_task');
    $query = $storage->getQuery();
    if ($project_id !== NULL) {
      $query->condition('project', $project_id);
    }
    if ($statuses) {
      $query->condition('status', $statuses, 'IN');
    }
    $query->sort('weight');
    $query->sort('label');
    $query->accessCheck(TRUE);
    $task_ids = $query->execute();
    return $storage->loadMultiple($task_ids);
  }

  /**
   * Get all epics for a project.
   *
   * @param int $project_id
   *   The project id.
   * @param array $statuses
   *   The statuses.
   *
   * @return array
   *   The tasks.
   */
  public function getEpics($project_id = NULL, $statuses = NULL) {
    $storage = $this->entityTypeManager->getStorage('kanban_epic');
    $query = $storage->getQuery();
    if ($project_id !== NULL) {
      $query->condition('project', $project_id);
    }
    if ($statuses) {
      $query->condition('status', $statuses, 'IN');
    }
    $query->sort('label');
    $query->accessCheck(TRUE);
    $task_ids = $query->execute();
    return $storage->loadMultiple($task_ids);
  }

  /**
   * Get rendered tasks.
   *
   * @param int $project_id
   *   The project id.
   * @param array $statuses
   *   The statuses.
   *
   * @return array
   *   The tasks in rendered form.
   */
  public function getRenderedTasks($project_id = NULL, $statuses = NULL) {
    $tasks = $this->getTasks($project_id, $statuses);
    $rendered_tasks = [];
    foreach ($tasks as $task) {
      $rendered_tasks[] = [
        '#theme' => 'kanban_board_task',
        '#task' => $task,
      ];
    }
    return $rendered_tasks;
  }

  /**
   * Get rendered epics.
   *
   * @param int $project_id
   *   The project id.
   * @param array $statuses
   *   The statuses.
   *
   * @return array
   *   The epics in rendered form.
   */
  public function getRenderedEpics($project_id = NULL, $statuses = NULL) {
    $epics = $this->getEpics($project_id, $statuses);
    $rendered_epics = [];
    foreach ($epics as $epic) {
      $rendered_epics[] = [
        '#theme' => 'kanban_board_epic',
        '#epic' => $epic,
      ];
    }
    return $rendered_epics;
  }

}
