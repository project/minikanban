<?php

namespace Drupal\minikanban\Form;

use Drupal\Core\Form\ConfigFormBase;

/**
 * The main configuration for the Kanban module.
 */
class KanbanForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'minikanban_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'minikanban.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('minikanban.settings');

    $form['kanban'] = [
      '#type' => 'details',
      '#title' => $this->t('Kanban settings'),
      '#open' => TRUE,
    ];

    $form['kanban']['administrator_theme'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use the administrator theme'),
      '#default_value' => $config->get('administrator_theme'),
      '#description' => $this->t('Use the administrator theme for the Kanban system.'),
    ];

    $form['mail'] = [
      '#type' => 'details',
      '#title' => $this->t('Mail settings'),
      '#open' => TRUE,
    ];

    $form['mail']['mail'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mail'),
      '#default_value' => $config->get('mail') ?? $this->config('system.site')->get('mail'),
      '#description' => $this->t('The outgoing mail for the Kanban system.'),
    ];

    $form['mail']['send_on_created'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send mail on task/epic created'),
      '#default_value' => $config->get('send_on_created') ?? TRUE,
      '#description' => $this->t('Send mail when a task is created.'),
    ];

    $form['mail']['send_on_changed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send mail on status changed'),
      '#default_value' => $config->get('send_on_status_changed') ?? TRUE,
      '#description' => $this->t('Send mail when a tasks status is changed.'),
    ];

    $form['mail']['send_on_comment'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send mail on comment'),
      '#default_value' => $config->get('send_on_comment') ?? TRUE,
      '#description' => $this->t('Send mail when a comment is added.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $this->config('minikanban.settings')
      ->set('mail', $form_state->getValue('mail'))
      ->set('administrator_theme', $form_state->getValue('administrator_theme'))
      ->set('send_on_created', $form_state->getValue('send_on_created'))
      ->set('send_on_changed', $form_state->getValue('send_on_changed'))
      ->set('send_on_comment', $form_state->getValue('send_on_comment'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
