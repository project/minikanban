<?php declare(strict_types = 1);

namespace Drupal\minikanban;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the epic entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class EpicAccessControlHandler extends KanbanAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    parent::checkAccess($entity, $operation, $account);
    return match($operation) {
      'view' => AccessResult::allowedIfHasPermissions($account, ['view kanban_epic', 'administer kanban_epic'], 'OR'),
      'update' => AccessResult::allowedIfHasPermissions($account, ['edit kanban_epic', 'administer kanban_epic'], 'OR'),
      'delete' => AccessResult::allowedIfHasPermissions($account, ['delete kanban_epic', 'administer kanban_epic'], 'OR'),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create kanban_epic', 'administer kanban_epic'], 'OR');
  }

}
