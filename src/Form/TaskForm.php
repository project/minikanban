<?php declare(strict_types = 1);

namespace Drupal\minikanban\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the task entity edit forms.
 */
final class TaskForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $form = parent::form($form, $form_state);
    $form['#attached']['library'][] = 'minikanban/styling';
    // Kanban numbero #1.
    $form['description']['widget'][0]['#format'] = 'kanban_html';
    $form['description']['widget'][0]['#allowed_formats'] = ['kanban_html'];
    // Nicer look
    foreach (['status', 'priority', 'assigned', 'story_points', 'project', 'reviewers', 'epic', 'due_date'] as $key) {
      $form[$key]['#prefix'] = '<div class="kanban-form-divider">';
      $form[$key]['#suffix'] = '</div>';
    }

    // Autofill project if in query string.
    $projectId = $this->getRequest()->query->get('project');
    if ($projectId) {
      $project = $this->entityTypeManager->getStorage('kanban_project')->load($projectId);
      if ($project->access('view')) {
        $form['project']['widget'][0]['target_id']['#default_value'] = $project;
      }
    }

    // Hide revision on creation form.
    if ($this->entity->isNew()) {
      $form['revision']['#access'] = FALSE;
      $form['revision_information']['#access'] = FALSE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $message_args = ['%label' => $this->entity->toLink()->toString()];
    $logger_args = [
      '%label' => $this->entity->label(),
      'link' => $this->entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New task %label has been created.', $message_args));
        $this->logger('minikanban')->notice('New task %label has been created.', $logger_args);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The task %label has been updated.', $message_args));
        $this->logger('minikanban')->notice('The task %label has been updated.', $logger_args);
        break;

      default:
        throw new \LogicException('Could not save the entity.');
    }

    $form_state->setRedirectUrl($this->entity->toUrl());

    return $result;
  }

  /**
   * Ajax callback for the task form.
   */
  public static function submit_task_ajax(array &$form, FormStateInterface $formState) {
    $response = new AjaxResponse();
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

}
