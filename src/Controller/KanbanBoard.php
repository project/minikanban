<?php

namespace Drupal\minikanban\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Drupal\minikanban\Entity\Project;
use Drupal\minikanban\ProjectHelper;
use Drupal\views\Plugin\views\argument_default\Raw;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class KanbanBoard.
 */
class KanbanBoard extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The project helper.
   *
   * @var \Drupal\minikanban\ProjectHelper
   */
  protected $projectHelper;

  /**
   * The renederer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The logged in user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Constructs a new KanbanBoard object.
   *
   * @param \Drupal\minikanban\ProjectHelper $project_helper
   *   The project helper.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The logged in user.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   */
  public function __construct(ProjectHelper $project_helper, Renderer $renderer, AccountProxyInterface $currentUser, FormBuilderInterface $formBuilder) {
    $this->projectHelper = $project_helper;
    $this->renderer = $renderer;
    $this->currentUser = $currentUser;
    $this->formBuilder = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('minikanban.project_helper'),
      $container->get('renderer'),
      $container->get('current_user'),
      $container->get('form_builder')
    );
  }

  /**
   * The Kanban Board.
   *
   * @param \Drupal\minikanban\Entity\Project $project
   *   The project.
   *
   * @return string
   *   Return Hello string.
   */
  public function board(Project $project = NULL) {
    // Get all projects.
    $projects = $this->projectHelper->getProjectsForOptions();
    // If its empty we ask to create a first project.
    if (count($projects) == 0) {
      // If the user can create projects.
      if ($this->currentUser->hasPermission('create kanban_project entities')) {
        Url::fromRoute('entity.kanban_project.add_form');
        $link = Link::createFromRoute('create your first project', 'entity.kanban_project.add_form');
        $link = $link->toRenderable();
        return [
          '#markup' => $this->t('No projects found. Please %link before you can see the Kanban board.', [
            '%link' => $this->renderer->render($link),
          ]),
        ];
      }
      else {
        // Otherwise just tell them to contact the admin.
        return [
          '#markup' => $this->t('No projects found for you. Please contact a Kanban administrator.'),
        ];
      }
    }

    // Get all tasks for the project.
    $tasks = $this->projectHelper->getRenderedTasks($project ? $project->id() : NULL);
    $epics = $this->projectHelper->getRenderedEpics($project ? $project->id() : NULL);
    $filterForm = $this->formBuilder->getForm('Drupal\minikanban\Form\TaskFilterForm');

    return [
      '#attached' => [
        'library' => [
          'minikanban/kanban_board',
          'minikanban/styling',
        ],
        'drupalSettings' => [
          'kanban' => [
            'project' => $project ? $project->id() : '',
          ],
        ],
      ],
      '#theme' => 'kanban_board_grid',
      '#tasks' => $tasks,
      '#epics' => $epics,
      '#filter_form' => $this->renderer->render($filterForm),
      '#cache' => ['max-age' => 0],
    ];
  }

  public function getProjectTitle(Project $project = NULL) {
    return $project ? $this->t('Kanban Board for %title', [
      '%title' => $project->label(),
     ]) : $this->t('Kanban Board');
  }

}
