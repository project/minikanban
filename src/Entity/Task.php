<?php declare(strict_types = 1);

namespace Drupal\minikanban\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\minikanban\TaskInterface;
use Drupal\user\Entity\User;
use Drupal\user\EntityOwnerTrait;

/**
 * Defines the task entity class.
 *
 * @ContentEntityType(
 *   id = "kanban_task",
 *   label = @Translation("Task"),
 *   label_collection = @Translation("Tasks"),
 *   label_singular = @Translation("task"),
 *   label_plural = @Translation("tasks"),
 *   label_count = @PluralTranslation(
 *     singular = "@count tasks",
 *     plural = "@count tasks",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\minikanban\TaskListBuilder",
 *     "views_data" = "Drupal\entity\EntityViewsData",
 *     "access" = "Drupal\minikanban\TaskAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\minikanban\Form\TaskForm",
 *       "edit" = "Drupal\minikanban\Form\TaskForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "kanban_task",
 *   revision_table = "kanban_task_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer kanban_task",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/kanban-task",
 *     "add-form" = "/kanban/task/add",
 *     "canonical" = "/kanban/task/{kanban_task}",
 *     "edit-form" = "/kanban/task/{kanban_task}/edit",
 *     "delete-form" = "/kanban/task/{kanban_task}/delete",
 *     "delete-multiple-form" = "/admin/content/kanban-task/delete-multiple",
 *   },
 *   field_ui_base_route = "entity.kanban_task.settings",
 * )
 */
final class Task extends RevisionableContentEntityBase implements TaskInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  public static array $taskStatuses = [
    'backlog' => 'Backlog',
    'blocked' => 'Blocked',
    'todo' => 'To do',
    'in_progress' => 'In progress',
    'in_review' => 'In review',
    'done' => 'Done',
    'closed' => 'Closed',
  ];

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);
    // Check if new.
    if (!$update && \Drupal::config('minikanban.settings')->get('send_on_created')) {
      // Send email.
      $mailSender = \Drupal::service('minikanban.mailsender');
      // Set html.
      $html = $this->description->value;
      $html .= '<br><br><hr><a href="' . $this->toUrl()->toString() . '">View task</a>';
      foreach ($this->getWatchers() as $watcher) {
        // Only send if they have one.
        $email = $watcher->getEmail();
        if ($email) {
          $title = $watcher == $this->getAssigned() ? '@author assigned you task "@task (@id)"' : 'Task "@task (@id)" created by @author';
          $mailSender->sendEmail(
            $email,
            t($title, [
              '@id' => $this->id(),
              '@task' => $this->label(),
              '@author' => $this->getOwner()->getDisplayName(),
            ]),
            $html,
          );
        }
      }
    }
    // Check if there are dependent blockers on this and set them todo.
    if ($this->status->value == 'done') {
      foreach ($this->getBlockingTasks() as $task) {
        // Check if all blockers are done and set to todo.
        foreach ($task->getBlockers() as $blocker) {
          if ($blocker->status->value != 'done') {
            continue 2;
          }
        }
        // If nothing found and its blocked, set to todo.
        if ($task->status->value == 'blocked') {
          $task->setStatus('todo');
          $task->save();
        }
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);
    $textHelper = $this->kanbanTextHelper();
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }

    // Get comments.
    $comments = $this->getComments();

    // Set attachments from comments and task.
    $files = $textHelper->grabFilesFromText($this->description->value);
    foreach ($comments as $comment) {
      $files = array_merge($files, $textHelper->grabFilesFromText($comment->comment->value));
    }
    $this->set('attachements', $files);

    // Count comments and attach.
    $this->set('comment_count', count($comments));

    // Find watchers.
    $watchers = [
      $this->getOwnerId(),
      $this->assigned->target_id
    ];

    // Reviewers.
    foreach ($this->reviewers as $reviewer) {
      $watchers[] = $reviewer->target_id;
    }

    // From description and comments.
    $watchers = array_merge($textHelper->grabUsersFromText($this->description->value), $watchers);
    foreach ($comments as $comment) {
      $watchers = array_merge($watchers, $textHelper->grabUsersFromText($comment->comment->value));
    }
    $this->set('watchers', array_unique($watchers));

  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    parent::delete();
    // Delete comments on task deletion.
    foreach ($this->getComments() as $comment) {
      $comment->delete();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Title'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Created by'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created on'))
      ->setDescription(t('The time that the task was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the task was last edited.'));

    $fields['due_date'] = BaseFieldDefinition::create('datetime')
      ->setLabel(t('Due Date'))
      ->setDescription(t('The time the task should be done.'))
      ->setDisplayOptions('form', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 45,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['story_points'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Story Points'))
      ->setDescription(t('The number of story points for the task.'))
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 25,
        'settings' => [
          'size' => 10,
          'placeholder' => '',
          'min' => '0',
          'max' => '1000',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['comment_count'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Comment Count'))
      ->setDescription(t('The number of comments.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'number_integer',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Description'))
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
        'settings' => [
          'rows' => 5,
          'default_format' => 'kanban_html',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['weight'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Weight'))
      ->setDescription(t('The weight of this task on the Kanban Board.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['project'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Project'))
      ->setSetting('target_type', 'kanban_project')
      ->setRequired(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'kanban_project',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Status'))
      ->setDefaultValue('backlog')
      ->setSettings([
        'allowed_values' => self::$taskStatuses,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['priority'] = BaseFieldDefinition::create('list_string')
      ->setLabel(t('Priority'))
      ->setDefaultValue('normal')
      ->setSettings([
        'allowed_values' => [
          'low' => t('Low'),
          'normal' => t('Normal'),
          'high' => t('High'),
          'critical' => t('Critical'),
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 17,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 17,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['assigned'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Assigned to'))
      ->setSetting('target_type', 'user')
        ->setDisplayOptions('form', [
          'type' => 'entity_reference_autocomplete',
          'settings' => [
            'match_operator' => 'CONTAINS',
            'size' => 60,
            'placeholder' => '',
          ],
          'weight' => 19,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('view', [
          'label' => 'above',
          'type' => 'author',
          'weight' => 30,
        ])
        ->setDisplayConfigurable('view', TRUE);

    $fields['reviewers'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Reviewers'))
      ->setSetting('target_type', 'user')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete_tags',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 30,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 30,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['watchers'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Watchers'))
      ->setSetting('target_type', 'user')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 35,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['epic'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Epic'))
      ->setSetting('target_type', 'kanban_epic')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 40,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 40,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['blockers'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Blockers'))
      ->setSetting('target_type', 'kanban_task')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 70,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'entity_reference_label',
        'weight' => 50,
        'settings' => [
          'link' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['attachements'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Attachements'))
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayConfigurable('form', TRUE)
      ->setSetting('file_extensions', 'txt docx doc docm dot dotm dotx odt pdf xml csv ods xls xlsb xlsm xlsx xlt xltx bmp jpg gif png jpeg odp pot ppa ppam pps ppt pptx rtf mp4 mov avi flv webm wmv zip tar mp3 aac flac wav wma')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'file_default',
        'weight' => 60,
        'settings' => [
          'use_description_as_link_text' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * Gets the description of the task.
   *
   * @return string
   *   The description of the task.
   */
  public function getDescription(): string {
    return $this->description->value ?? '';
  }

  /**
   * Gets the priority of the task.
   *
   * @return string
   *   The priority of the task.
   */
  public function getPriority(): string {
    return $this->priority->value ?? '';
  }

  /**
   * Gets the due date.
   *
   * @return string
   *   The due date of the task.
   */
  public function getDueDate(): string {
    return $this->due_date->value ?? '';
  }

  /**
   * Gets the due date formatted.
   *
   * @return string
   *   The due date of the task formatted.
   */
  public function getDueDateFormatted(): string {
    return $this->due_date->value ? date('Y-m-d', strtotime($this->due_date->value)) : '';
  }

  /**
   * Gets the assigned person.
   *
   * @return \Drupal\user\Entity\User|null
   *   The assigned person.
   */
  public function getAssigned(): User|null {
    return $this->assigned->entity ? $this->assigned->entity : NULL;
  }

  /**
   * Gets the project.
   *
   * @return \Drupal\minikanban\Entity\Project|null
   *   The project.
   */
  public function getProject(): Project|null {
    return $this->project->entity ? $this->project->entity : NULL;
  }

  /**
   * Gets the epic.
   *
   * @return \Drupal\minikanban\Entity\Epic|null
   *   The epic.
   */
  public function getEpic(): Epic|null {
    return $this->epic->entity ?? NULL;
  }

  /**
   * Gets the comment count.
   *
   * @return int
   *   The comment count.
   */
  public function getCommentCount(): int {
    return (int) $this->comment_count->value ?? 0;
  }

  /**
   * Gets the Kanban Text Helper.
   *
   * @return \Drupal\minikanban\TextHelper
   */
  protected function kanbanTextHelper() {
    return \Drupal::service('minikanban.text_helper');
  }

  /**
   * Get the authors username.
   *
   * @return string
   *   The authors username.
   */
  public function getAuthorsUsername(): string {
    return $this->getOwner()->getDisplayName();
  }

  /**
   * Get the title.
   *
   * @return string
   *   The title.
   */
  public function getTitle(): string {
    return $this->label();
  }

  /**
   * Gets the watchers.
   *
   * @return array
   *   The watchers.
   */
  public function getWatchers(): array {
    $watchers = [];
    foreach ($this->watchers as $watcher) {
      $watchers[] = $watcher->entity;
    }
    return $watchers;
  }

  /**
   * Gets the comments for a task.
   *
   * @return array
   *   The comments for a task.
   */
  public function getComments(): array {
    $comments = [];
    $commentStorage = $this->entityTypeManager()->getStorage('kanban_comment');
    $query = $commentStorage->getQuery()
      ->condition('task', $this->id())
      ->accessCheck(TRUE);
    $comment_ids = $query->execute();
    if ($comment_ids) {
      $comments = $commentStorage->loadMultiple($comment_ids);
    }
    return $comments;
  }

  /**
   * Adds a comment.
   *
   * @param string $comment
   *   The comment to add.
   * @param int $uid
   *   The user id of the author.
   *
   * @return \Drupal\minikanban\Entity\Comment
   *   The comment entity.
   */
  public function addComment(string $comment, $uid) {
    $commentStorage = $this->entityTypeManager()->getStorage('kanban_comment');
    $comment = $commentStorage->create([
      'task' => $this->id(),
      'uid' => 2,
      'comment' => [
        'value' => $comment,
        'format' => 'kanban_html',
      ],
    ])->save();
    return $comment;
  }

  /**
   * Give back tasks that this task is blocking.
   *
   * @return array
   *   The tasks that this task is blocking.
   */
  public function getBlockingTasks(): array {
    $storage = \Drupal::entityTypeManager()->getStorage('kanban_task');
    $query = $storage->getQuery()
      ->condition('blockers', $this->id())
      ->accessCheck(TRUE);
    $task_ids = $query->execute();
    return $storage->loadMultiple($task_ids);
  }

  /**
   * Gets the blockers.
   *
   * @return array
   *   The blockers for this task.
   */
  public function getBlockers(): array {
    $blockers = [];
    foreach ($this->blockers as $blocker) {
      $blockers[] = $blocker->entity;
    }
    return $blockers;
  }

  /**
   * Gets the attachements.
   *
   * @return array
   *   The attachements for this task.
   */
  public function getAttachements(): array {
    $attachements = [];
    foreach ($this->attachements as $attachement) {
      $attachements[] = $attachement->entity;
    }
    return $attachements;
  }

  /**
   * Sets the status of the task.
   *
   * @param string $status
   *   The status of the task.
   */
  public function setStatus(string $status): void {
    $this->set('status', $status);
  }

  /**
   * Set the assignee of the task.
   *
   * @param \Drupal\user\Entity\User $user
   *   The user to assign the task to.
   */
  public function setAssignee(User $user): void {
    $this->set('assigned', $user->id());
  }

}
