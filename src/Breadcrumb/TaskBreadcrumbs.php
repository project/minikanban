<?php

namespace Drupal\minikanban\Breadcrumb;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;

class TaskBreadcrumbs implements BreadcrumbBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    return $route_match->getRouteName() == 'entity.kanban_task.canonical';
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $task = $route_match->getParameter('kanban_task');
    $breadcrumb->addLink(Link::createFromRoute('Home', '<front>'));
    $breadcrumb->addLink(Link::createFromRoute('Kanban', 'minikanban.kanban_board'));
    $project = $task->getProject();
    $breadcrumb->addLink(Link::createFromRoute($project->label(), 'entity.kanban_project.canonical', ['kanban_project' => $project->id()]));
    if (!empty($task->epic->entity)) {
      $epic = $task->epic->entity;
      $breadcrumb->addLink(Link::createFromRoute($epic->label(), 'entity.kanban_epic.canonical', ['kanban_epic' => $epic->id()]));
    }
    return $breadcrumb;
  }

}
