<?php

namespace Drupal\minikanban\Menu;

use Drupal\Core\Menu\LocalActionDefault;
use Drupal\Core\Menu\LocalActionInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Provides a local task for the kanban task add form.
 */
final class KanbanAddFormBoard extends LocalActionDefault implements LocalActionInterface {

  /**
   * {@inheritdoc}
   */
  public function getRouteParameters(RouteMatchInterface $routeMatch): array {
    $project = $routeMatch->getRawParameters()->get('project') ?? NULL;
    return [
      'project' => $project,
      'destination' => '/kanban/' . $project,
    ];
  }
}
