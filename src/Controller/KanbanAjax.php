<?php

namespace Drupal\minikanban\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Render\Renderer;
use Drupal\minikanban\Entity\Epic;
use Drupal\minikanban\Entity\Task;
use Drupal\minikanban\ProjectHelper;
use Drupal\minikanban\TaskHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class KanbanAjax.
 */
class KanbanAjax extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The task helper.
   *
   * @var \Drupal\minikanban\TaskHelper
   */
  protected $taskHelper;

  /**
   * The project helper.
   *
   * @var \Drupal\minikanban\ProjectHelper
   */
  protected $projectHelper;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new KanbanAjax object.
   *
   * @param \Drupal\minikanban\TaskHelper $taskHelper
   *   The task helper.
   * @param \Drupal\minikanban\ProjectHelper $projectHelper
   *   The project helper.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(TaskHelper $taskHelper, ProjectHelper $projectHelper, Renderer $renderer, FormBuilderInterface $formBuilder, EntityTypeManagerInterface $entityTypeManager) {
    $this->taskHelper = $taskHelper;
    $this->projectHelper = $projectHelper;
    $this->renderer = $renderer;
    $this->formBuilder = $formBuilder;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('minikanban.task_helper'),
      $container->get('minikanban.project_helper'),
      $container->get('renderer'),
      $container->get('form_builder'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * A task update.
   *
   * @param \Drupal\minikanban\Entity\Task $task
   *   The task.
   * @param string $status
   *   The status.
   * @param int $weight
   *   The weight.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   Return if it worked.
   */
  public function updateTaskStatus(Task $task, $status, $weight) {
    // Underscore if not.
    $status = str_replace('-', '_', $status);
    // Check so its an allowed status.
    if (!isset(Task::$taskStatuses[$status])) {
      return $this->setError('Invalid status.', 422);
    }
    // Update the status of the task.
    try {
      $this->taskHelper->updateTaskStatus($task, $status, $weight);
      return new JsonResponse([
        'status' => 'ok',
        'message' => 'Task status updated.',
      ]);
    }
    catch (\Exception $e) {
      // Catch error if something happened.
      return $this->setError($e->getMessage(), 400);
    }
  }

  /**
   * Update multiple tasks from POST.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   Return if it worked.
   */
  public function updateTaskStatusMultiple(Request $request) {
    // Get the tasks from the post.
    $tasks = $request->get('tasks');

    // Check so we have tasks.
    if (empty($tasks)) {
      return $this->setError('No tasks to update.', 422);
    }
    try {
      // Loop through the tasks and update them.
      foreach ($tasks as $task) {
        $status = str_replace('-', '_', $task['status']);
        if (!isset(Task::$taskStatuses[$status])) {
          return $this->setError('Invalid status.', 422);
        }
        $taskEntity = $this->entityTypeManager->getStorage('kanban_task')->load($task['id']);
        $this->taskHelper->updateTaskStatus($taskEntity, $status, $task['weight']);
      }
      return new JsonResponse([
        'status' => 'ok',
        'message' => 'Tasks updated.',
      ]);
    } catch (\Exception $e) {
      // Catch error if something happened.
      return $this->setError($e->getMessage(), 400);
    }

  }

  /**
   * Update multiple epic from POST.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   Return if it worked.
   */
  public function updateEpicStatusMultiple(Request $request) {
    // Get the epics from the post.
    $epics = $request->get('epics');

    // Check so we have epics.
    if (empty($epics)) {
      return $this->setError('No epics to update.', 422);
    }
    try {
      // Loop through the epics and update them.
      foreach ($epics as $epic) {
        $status = str_replace('-', '_', $epic['status']);
        if (!isset(Epic::$epicStatuses[$status])) {
          return $this->setError('Invalid status, Epics can not be blocked.', 422);
        }
        $epicEntity = $this->entityTypeManager->getStorage('kanban_epic')->load($epic['id']);
        $epicEntity->set('status', $status);
        $epicEntity->save();
      }
      return new JsonResponse([
        'status' => 'ok',
        'message' => 'Epics updated.',
      ]);
    } catch (\Exception $e) {
      // Catch error if something happened.
      return $this->setError($e->getMessage(), 400);
    }
  }

  /**
   * Get all the tasks via ajax.
   *
   * @param \Drupal\minikanban\Entity\Project $project
   *   The project.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   Return the tasks.
   */
  public function getBoard($project = NULL) {
    // Get all tasks for the project.
    $tasks = $this->projectHelper->getRenderedTasks($project ? $project->id() : NULL);
    $epics = $this->projectHelper->getRenderedEpics($project ? $project->id() : NULL);
    $filterForm = $this->formBuilder->getForm('Drupal\minikanban\Form\TaskFilterForm');
    $newBoard = [
      '#attached' => [
        'library' => [
          'minikanban/kanban_board',
          'minikanban/styling',
        ],
      ],
      '#theme' => 'kanban_board_grid',
      '#tasks' => $tasks,
      '#epics' => $epics,
      // We don't update the filter form.
      '#filter_form' => NULL,
      '#cache' => ['max-age' => 0],
    ];
    $newBoard = $this->renderer->render($newBoard);
    return new JsonResponse([
      'status' => 'ok',
      'message' => 'Tasks updated.',
      'board' => $newBoard,
    ]);
  }

  /**
   * Helper function to set error message and code.
   *
   * @param string $message
   *   The message.
   * @param int $code
   *   The code.
   *
   * @return Symfony\Component\HttpFoundation\JsonResponse
   *   Return error output.
   */
  public function setError($message, $code) {
    return new JsonResponse([
      'status' => 'error',
      'message' => $message,
    ], $code);
  }

}
