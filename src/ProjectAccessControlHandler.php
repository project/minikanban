<?php declare(strict_types = 1);

namespace Drupal\minikanban;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the project entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class ProjectAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
    // Administrators or special solution sees all.
    if ($account->hasPermission('administer kanban')) {
      return AccessResult::allowed();
    }
    if ($account->hasPermission('view all kanban projects') && $operation === 'view') {
      return AccessResult::allowed();
    }
    // If the user is a member of the project, then they can see the entity.
    $found = FALSE;
    foreach ($entity->members as $member) {
      if ($member->entity->id() === $account->id()) {
        $found = TRUE;
      }
    }
    if (!$found) {
      return AccessResult::forbidden();
    }
    return match($operation) {
      'view' => AccessResult::allowedIfHasPermissions($account, ['view kanban_project', 'administer kanban_project'], 'OR'),
      'update' => AccessResult::allowedIfHasPermissions($account, ['edit kanban_project', 'administer kanban_project'], 'OR'),
      'delete' => AccessResult::allowedIfHasPermissions($account, ['delete kanban_project', 'administer kanban_project'], 'OR'),
      default => AccessResult::neutral(),
    };
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create kanban_project', 'administer kanban_project'], 'OR');
  }

}
