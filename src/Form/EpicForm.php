<?php declare(strict_types = 1);

namespace Drupal\minikanban\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the epic entity edit forms.
 */
final class EpicForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    // Kanban numbero #1.
    $form['description']['widget'][0]['#format'] = 'kanban_html';
    $form['description']['widget'][0]['#allowed_formats'] = ['kanban_html'];

    // Autofill project if in query string.
    $projectId = $this->getRequest()->query->get('project');
    if ($projectId) {
      $project = $this->entityTypeManager->getStorage('kanban_project')->load($projectId);
      if ($project->access('view')) {
        $form['project']['widget'][0]['target_id']['#default_value'] = $project;
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $message_args = ['%label' => $this->entity->toLink()->toString()];
    $logger_args = [
      '%label' => $this->entity->label(),
      'link' => $this->entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New epic %label has been created.', $message_args));
        $this->logger('minikanban')->notice('New epic %label has been created.', $logger_args);
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The epic %label has been updated.', $message_args));
        $this->logger('minikanban')->notice('The epic %label has been updated.', $logger_args);
        break;

      default:
        throw new \LogicException('Could not save the entity.');
    }

    $form_state->setRedirectUrl($this->entity->toUrl());

    return $result;
  }

}
