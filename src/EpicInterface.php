<?php declare(strict_types = 1);

namespace Drupal\minikanban;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an epic entity type.
 */
interface EpicInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

}
